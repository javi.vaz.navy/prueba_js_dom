"use strict";

function decimalToBinario(num) {
  const bin = num.toString(2);
  return bin;
}

function binarioToDecimal(num) {
  const dec = Number("0b" + num);
  return dec;
}

const number = Number(prompt(`introduce el numero que quieres convertir:`));

const base = Number(
  prompt(`introduce 2 si el numero es binario y 10 si es decimal.`)
);

if (base === 2) console.log(binarioToDecimal(number));
if (base === 10) console.log(decimalToBinario(number));
if (base !== 2 && base !== 10) console.error(`numero invalido`);

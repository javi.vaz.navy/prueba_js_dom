"use strict";

const url = "https://rickandmortyapi.com/api/";

const urlCharacters = "https://rickandmortyapi.com/api/character/";

function deleteCopys(object) {
  for (let j = 0; j < object.length; j++) {
    for (let i = j + 1; i < object.length; i++) {
      if (object[j] === object[i]) object.splice(i, 1);
    }
  }
  return object;
}

async function getCharacters(URL) {
  try {
    const response = await fetch(URL);
    const data = await response.json();

    const episodes = await fetch(data.episodes);
    const episodesPage1 = await episodes.json();

    const charactersJanuary = [];
    const nameCharacters = [];
    const numberCharacters = [];

    for (let i = 0; i < episodesPage1.results.length; i++) {
      const fechas = episodesPage1.results[i].air_date.split(" ");

      if (fechas[0] === "January") {
        const characters = episodesPage1.results[i].characters;
        charactersJanuary.push(characters);
      }
    }

    for (let i = 0; i < charactersJanuary.length; i++) {
      for (let j = 0; j < charactersJanuary[i].length; j++) {
        const character = charactersJanuary[i][j];
        const char = character.split("/");
        numberCharacters.push(char[5]);
      }
    }

    const numCharactersFinal = deleteCopys(numberCharacters);

    for (let i = 0; i < numCharactersFinal.length; i++) {
      const charac = await fetch(urlCharacters + numCharactersFinal[i]);
      const charact = await charac.json();

      nameCharacters.push(charact.name);
    }

    console.log(deleteCopys(nameCharacters));
  } catch (err) {
    console.error("ERROR");
  }
}

getCharacters(url);

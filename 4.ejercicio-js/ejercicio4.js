"use strict";

const names = [
  "A-Jay",
  "Manuel",
  "Manuel",
  "Eddie",
  "A-Jay",
  "Su",
  "Reean",
  "Manuel",
  "A-Jay",
  "Zacharie",
  "Zacharie",
  "Tyra",
  "Rishi",
  "Arun",
  "Kenton",
];

function deleteCopys(object) {
  for (let j = 0; j < object.length; j++) {
    for (let i = j + 1; i < object.length; i++) {
      if (object[j] === object[i]) object.splice(i, 1);
    }
  }
  return object;
}

console.log(deleteCopys(names));

"use strict";

function timeTest() {
  let sec = 0;
  let min = 0;
  let hours = 0;
  let days = 0;
  setInterval(() => {
    if (sec < 55) {
      sec += 5;
    } else {
      sec = 0;
      min += 1;
    }

    if (min === 55) {
      min = 0;
      hours += 1;
    }

    if (hours === 23) {
      hours = 0;
      days += 1;
    }

    console.log(
      `han pasado ${days} dias, ${hours} horas, ${min} minutos y ${sec} segundos`
    );
  }, 5000);
}

timeTest();

const addCuad = document.querySelector("button.add");

const getRandomValue = (max, min = 0) =>
  Math.floor(Math.random() * (max + 1 - min) + min);

setInterval(() => {
  const cuadrados = document.querySelectorAll("div.cuadrado");
  for (const cuad of cuadrados) {
    cuad.style.backgroundColor = ` rgb(
                ${getRandomValue(255)},
                ${getRandomValue(255)},
                ${getRandomValue(255)})
                `;
  }
}, 1000);

const handleAddCuadrado = (e) => {
  e.preventDefault();

  const newCuadrado = document.createElement("div");
  const malla = document.querySelector(".malla");

  newCuadrado.classList.add("cuadrado");

  malla.append(newCuadrado);
};

addCuad.addEventListener("click", handleAddCuadrado);

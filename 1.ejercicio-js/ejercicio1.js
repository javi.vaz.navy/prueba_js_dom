"use strict";

const numUsers = Number(prompt(`Cuantos usuarios quieres?`));

async function getUsers(num) {
  try {
    const response = await fetch(`https://randomuser.me/api/?results=${num}`);

    const data = await response.json();

    const newArray = [];

    for (const user of data.results) {
      const usuario = {
        name: user.name.first,
        lastName: user.name.last,
        gender: user.gender,
        country: user.location.country,
        email: user.email,
        picture: user.picture.large,
      };

      newArray.push(usuario);
    }

    console.log(newArray);
  } catch (error) {
    console.log(error);
  }
}

getUsers(numUsers);

const parrafo1 = document.querySelector("p.parrafo1");
let texto1 = parrafo1.innerHTML;
let array1 = texto1.split(/[\s,\.,\"]+/);

array1.forEach((word) => {
  if (word.length > 5) {
    texto1 = texto1.split(word).join(`<u>${word}</u>`);
  }
});
parrafo1.innerHTML = texto1;

const parrafo2 = document.querySelector("p.parrafo2");
let texto2 = parrafo2.innerHTML;
let array2 = texto2.split(/[\s,\.,\"]+/);

array2.forEach((word) => {
  if (word.length > 5) {
    texto2 = texto2.split(word).join(`<u>${word}</u>`);
  }
});
parrafo2.innerHTML = texto2;

const parrafo3 = document.querySelector("p.parrafo3");
let texto3 = parrafo3.innerHTML;
let array3 = texto3.split(/[\s,\.,\"]+/);

array3.forEach((word) => {
  if (word.length > 5) {
    texto3 = texto3.split(word).join(`<u>${word}</u>`);
  }
});
parrafo3.innerHTML = texto3;

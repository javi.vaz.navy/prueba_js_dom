const crono = document.querySelector("body>header>h1");
const start = document.querySelector(".start");
const pause = document.querySelector(".pause");
const reset = document.querySelector(".reset");

let segundos = 0;
let minutos = 0;
let horas = 0;

let interval = "";

const handleClickStartButton = (e) => {
  e.preventDefault();

  interval = setInterval(function () {
    if (segundos <= 58) {
      segundos += 1;
    } else {
      segundos = 0;
      minutos += 1;
    }

    if (minutos <= 58) {
      minutos += 0;
    } else {
      minutos = 0;
      horas += 1;
    }

    const sec = segundos < 10 ? "0" + segundos : segundos;
    const min = minutos < 10 ? "0" + minutos : minutos;
    const hour = horas < 10 ? "0" + horas : horas;

    if (segundos % 2 === 0) {
      crono.textContent = `${hour}:${min}:${sec}`;
    } else {
      crono.textContent = `${hour} ${min} ${sec}`;
    }
  }, 1000);
};

const handleClickPauseButton = (e) => {
  e.preventDefault();
  clearInterval(interval);
};

const handleClickResetButton = (e) => {
  e.preventDefault();
  clearInterval(interval);

  minutos = 0;
  segundos = 0;
  horas = 0;

  const sec = segundos < 10 ? "0" + segundos : segundos;
  const min = minutos < 10 ? "0" + minutos : minutos;
  const hour = horas < 10 ? "0" + horas : horas;

  if (segundos % 2 === 0) {
    crono.textContent = `${hour}:${min}:${sec}`;
  } else {
    crono.textContent = `${hour} ${min} ${sec}`;
  }
};

pause.addEventListener("click", handleClickPauseButton);
start.addEventListener("click", handleClickStartButton);
reset.addEventListener("click", handleClickResetButton);
